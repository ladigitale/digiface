# Digiface

Digiface est une application en ligne pour créer des avatars. 

Elle est publiée sous licence GNU AGPLv3.
Sauf la fonte Material Icons (Apache License Version 2.0) et la fonte Mona Sans Expanded (Sil Open Font Licence 1.1)

### Préparation et installation des dépendances
```
npm install
```

### Lancement du serveur de développement
```
npm run dev
```

### Compilation et minification des fichiers
```
npm run build
```

### Production
Le dossier dist peut être déployé directement sur un serveur de fichier.

### Démo
https://ladigitale.dev/digiface/

### Remerciements et crédits
Traduction en italien par des élèves de Luc Chaideyrou, enseignant d'italien à Lyon

### Soutien
Open Collective : https://opencollective.com/ladigitale

Liberapay : https://liberapay.com/ladigitale/
